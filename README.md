# Vidyo.io Android SDK Package

Contains all related *.so native libraries and java *.jar library.

Compiled into aar package. Add to the project as dependency:

## maven ##

    <dependency>
      <groupId>com.vidyo.io.client</groupId>
      <artifactId>android</artifactId>
      <version>4.1.24.9</version>
      <type>pom</type>
    </dependency>

## gradle ##

    implementation 'com.vidyo.io.client:android:4.1.24.9'

* On gradle prior to 3.x version use 'compile' instead of 'implementation'

## API Documentation ##

https://developer.vidyo.io

## Enjoy! ##
