package com.vidyo.app;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.vidyo.VidyoClient.Connector.Connector;
import com.vidyo.VidyoClient.Connector.ConnectorPkg;

import java.util.ArrayList;
import java.util.List;

public class ConfigActivity extends FragmentActivity {

    private static final String[] PERMISSIONS = new String[]{
            Manifest.permission.CAMERA,
            Manifest.permission.RECORD_AUDIO
    };

    private static final int PERMISSIONS_REQUEST_ALL = 0x7c9;

    private Connector connector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);
        requestPermissions();
    }

    private void configure() {
        FrameLayout videoView = findViewById(R.id.video_view);

        boolean init = ConnectorPkg.initialize();
        ConnectorPkg.setApplicationUIContext(this);

        TextView status = findViewById(R.id.status_tv);

        this.connector = new Connector(videoView, Connector.ConnectorViewStyle.VIDYO_CONNECTORVIEWSTYLE_Default, 0, "", "", 0);

        status.setText("Vidyo.io initialize status: " + init + "\nVersion: " + connector.getVersion());

        videoView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                connector.showViewAt(v, 0, 0, v.getWidth(), v.getHeight());
            }
        });
    }

    private void requestPermissions() {
        if (Build.VERSION.SDK_INT < 23) {
            configure();
            return;
        }

        List<String> permissionsNeeded = new ArrayList<>();
        for (String permission : PERMISSIONS) {
            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED)
                permissionsNeeded.add(permission);
        }

        if (permissionsNeeded.size() > 0) {
            ActivityCompat.requestPermissions(this, permissionsNeeded.toArray(new String[0]), PERMISSIONS_REQUEST_ALL);
        } else {
            configure();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_ALL) {
            requestPermissions();
        }
    }
}